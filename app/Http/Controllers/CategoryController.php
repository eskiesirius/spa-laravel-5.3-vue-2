<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Category;

class CategoryController extends Controller
{
    public function index()
    {
        return Category::all();
    }

    public function topics($categoryId)
    {
        $topics = Category::findOrFail($categoryId)
        ->topics()
        ->orderBy('created_at', 'desc')
        ->get();

        if ($topics->isEmpty()) {
            return response('The provided page exceeds the available number of pages', 404);
        }

        return $topics;
    }
}
