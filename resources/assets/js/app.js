
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
var VueRouter = require('vue-router/dist/vue-router.js');
Vue.use(VueRouter);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));
Vue.component('App', require('./components/App.vue'));

const HomeView = Vue.component('HomeView', require('./components/HomeView.vue'));
const Category = Vue.component('Category', require('./components/Category.vue'));
const CategoryView = Vue.component('CategoryView', require('./components/CategoryView.vue'));
const Topic = Vue.component('CategoryView', require('./components/Topic.vue'));

const Foo = { template: '<div>foo</div>' };
const Bar = { template: '<div>bar</div>' };

const routes = [
	{ path: '/', name:'Home',component: HomeView },
	{ path: '/category/:categoryId', name:'category', component: CategoryView },
	{ path: '*', redirect: { name: 'Home' } }
];

const router = new VueRouter({
	routes, // short for routes: routes
	mode: 'history'
});

const app = new Vue({
	router
}).$mount('#app');