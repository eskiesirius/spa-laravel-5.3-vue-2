<?php

use Illuminate\Database\Seeder;
use App\Topic;
use App\Category;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
    	$this->call(CategoriesTopicsSeeder::class);
    }
}

class CategoriesTopicsSeeder extends Seeder
{
	
	public function run()
	{
		Eloquent::unguard();

		DB::statement('SET FOREIGN_KEY_CHECKS=0;');

		Topic::truncate();
		Category::truncate();

		factory(Category::class, 5)->create()->each(function($c) {
			$c->topics()->saveMany(
				factory(Topic::class, 5)->create([
					'category_id' => $c->id
					])
				);
		});

		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
	}
}
